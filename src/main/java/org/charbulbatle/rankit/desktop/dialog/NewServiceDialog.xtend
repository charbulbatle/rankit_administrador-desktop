package org.charbulbatle.rankit.desktop.dialog

import org.uqbar.arena.windows.Dialog
import org.charbulbatle.rankit.app_models.CreateServiceAppModel
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.widgets.Button
import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*

class NewServiceDialog extends Dialog<CreateServiceAppModel>{
	
	new(WindowOwner owner, CreateServiceAppModel model) {
		super(owner, model)
	}
	
	override protected createErrorsPanel(Panel mainPanel) {
		//
	}
	
	override protected createFormPanel(Panel mainPanel) {
		this.title = "Agregar Servicio"
		
		new Label(mainPanel).text = "Ingrese el nombre del servicio:"
		
		new TextBox(mainPanel) => [
			value <=> "name"
		]
		
		new Button(mainPanel) =>[
			caption = "Aceptar"
			enabled <=> "nombreIngresado"
			onClick[| 
				this.modelObject.createService;
				this.close
			]
		]
	}
}