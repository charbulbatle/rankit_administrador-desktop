package org.charbulbatle.rankit.desktop.dialog

import org.charbulbatle.rankit.app_models.CreateUserAppModel
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.windows.Dialog
import org.uqbar.arena.windows.WindowOwner

import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*

class NewUserDialog extends Dialog<CreateUserAppModel>{
	
	new(WindowOwner owner, CreateUserAppModel model) {
		super(owner, model)
	}
	
	override protected createErrorsPanel(Panel mainPanel) {
		//
	}
	
	override protected createFormPanel(Panel mainPanel) {
		this.title = "Agregar Usuario"
		
		new Label(mainPanel).text = "Ingrese el nombre de usuario:"
		
		new TextBox(mainPanel) => [
			value <=> "name"
		]
		
		new Button(mainPanel) =>[
			caption = "Aceptar"
			enabled <=> "nombreIngresado"
			onClick[| 
				this.modelObject.createUser;
				this.close
			]
		]
	}
	
	
}