package org.charbulbatle.rankit.desktop.dialog

import org.charbulbatle.rankit.utils.Deletable
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.Dialog
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.windows.ErrorsPanel

class DeleteDialog extends Dialog<Deletable>{
	
	new(WindowOwner owner, Deletable model) {
		super(owner, model)
	}
	
	override protected createFormPanel(Panel mainPanel) {
		new Label(mainPanel).text = "Seguro que quiere eliminar este elemento?"
		var horizontal = new Panel(mainPanel) => [
			layout = new HorizontalLayout
		]
		
		new Button(horizontal) => [
			caption = "Aceptar"
			onClick [ | this.accept]
		]
		
		new Button(horizontal) => [
			caption = "Cancelar"
			onClick [ | this.cancel]
		]
		
		this.title = "Eliminar Elemento"
		
	}
	
	override ErrorsPanel createErrorsPanel(Panel mainPanel) {
		return null
	}
	
	override executeTask(){
		super.executeTask
		this.modelObject.eliminar
		
	}
	
}