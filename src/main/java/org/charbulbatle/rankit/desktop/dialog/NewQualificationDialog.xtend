package org.charbulbatle.rankit.desktop.dialog
import org.uqbar.arena.windows.Dialog
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.widgets.Panel
import org.charbulbatle.rankit.domain.User
import org.charbulbatle.rankit.app_models.UserAppModel
import org.charbulbatle.rankit.app_models.CreateUserAppModel
import org.uqbar.arena.widgets.TextBox
import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Button
import org.charbulbatle.rankit.app_models.CreateQualificationAppModel
import org.uqbar.arena.widgets.Selector
import org.charbulbatle.rankit.domain.Qualification
import org.uqbar.arena.bindings.ValueTransformer
import org.uqbar.arena.bindings.Adapter
import org.uqbar.lacar.ui.model.BindingBuilder

class NewQualificationDialog extends Dialog<CreateQualificationAppModel> {
	
	new(WindowOwner owner, CreateQualificationAppModel model) {
		super(owner, model)
	}
	
	override protected createErrorsPanel(Panel mainPanel) {
		//
	}
	
	override protected createFormPanel(Panel mainPanel) {
		this.title = "Agregar Calificacion"
		
		new Label(mainPanel).text = "Seleccione el Evaluado: "
		
		new Selector<Qualification>(mainPanel) => [
			allowNull(false)
			value <=> "name"
			bindItemsToProperty("calificados")
			
		]
			
		new Button(mainPanel) =>[
			caption = "Aceptar"
			enabled <=> "nombreIngresado"
			onClick[| 
				this.modelObject.createQualification;
				this.close
			]
		]
	}
	
}
