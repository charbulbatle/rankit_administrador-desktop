package org.charbulbatle.rankit.desktop.window

import java.awt.Color
import org.charbulbatle.rankit.app_models.QualificationAppModel
import org.charbulbatle.rankit.desktop.components.LabeledCheckBox
import org.charbulbatle.rankit.desktop.components.LabeledTextBox
import org.charbulbatle.rankit.desktop.components.LabeledValue
import org.charbulbatle.rankit.utils.DummyData
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.widgets.tables.Table
import org.uqbar.arena.windows.WindowOwner

import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import org.charbulbatle.rankit.domain.Qualification
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import org.charbulbatle.rankit.desktop.dialog.NewQualificationDialog
import org.charbulbatle.rankit.app_models.CreateQualificationAppModel
import org.charbulbatle.rankit.desktop.dialog.DeleteDialog
import org.uqbar.arena.widgets.NumericField

class QualificationsManagementWindow extends AdministrationWindow<QualificationAppModel> {
	
	new(WindowOwner owner, QualificationAppModel model) {
		super(owner, model)
	}
	
	override crearFeedBackTitle(Panel panel) {
		new Label(panel) =>[
			text = ""
		]
		
		// titulo
		this.title = "RankIt --> Adm Calificaciones"
	}
	
	override crearResumenDeSituacion(Panel mainPanel) {
		val panelHorizontal = new Panel(mainPanel) =>[
			layout = new HorizontalLayout
		]
		new LabeledValue(panelHorizontal, Color.BLUE, "Calificaciones Registradas: ") =>[
			value.value <=> "calificacionesTotales"
		]
		new LabeledValue(panelHorizontal, Color.RED, "Ofensivas: ") =>[
			value.value <=> "calificacionesOfensivas"
		]
	}
	
	override crearBuscador(Panel panel) {
		val contenedorCalificaciones = new Panel(panel) => [
			layout = new HorizontalLayout
		]
		
		new Label(contenedorCalificaciones) => [
			text 	 = "Usuarios"
			fontSize = 16
		]
		
		// Buscador /////////////////////////////////
		val buscador = new Panel(panel) =>[
			layout = new HorizontalLayout
		]
		
		new LabeledTextBox(panel, "Usuario") =>[
			textBox.value <=> "usuario"
		]
		
		new LabeledTextBox(panel, "Evaluado") =>[
			textBox.value <=> "evaluado"
		]
		
		// /////////////////////////////////////////
	}
	
	override crearBotonDeNuevo(Panel panel) {
		new Button(panel) => [
			caption = "Nuevo"
			onClick [ | new NewQualificationDialog(this, new CreateQualificationAppModel(modelObject)).open]
		]
	}
	
	override crearPanelDeAcciones(Panel panel) {
		new Label(panel) =>[
			text = "Evaluado"
		]
		
		new Label(panel) =>[
			value <=> "seleccionado.evaluado"
		]
		
		new LabeledValue(panel, Color.BLACK) =>[
			label.text = "Fecha: "
			value.value <=> "seleccionado.fecha"
		]
		
		new LabeledValue(panel, Color.BLACK) =>[
			label.text = "Usuario: "
			value.value <=> "seleccionado.user"
		]
		
		new Label(panel).text = "Puntaje: "
		
		new NumericField(panel).value <=> "seleccionado.puntos"
		
		new Label(panel).text = "Detalle"
		
		new TextBox(panel) =>[
			width = 400
			height = 100
			value <=> "seleccionado.detalle"
		]
		
		new LabeledCheckBox(panel, "Contenido Ofensivo") =>[
			checkbox.value <=> "tieneContenidoOfensivo"
			checkbox.enabled <=> "haySeleccionado"
		]
		
		new Button(panel) => [
			caption = "Eliminar"
			enabled <=> "haySeleccionado"
			onClick [ | new DeleteDialog(this, this.modelObject).open]
		]
		
	}
	
	override crearTabla(Panel panel) {
		val tabla = new Table<Qualification>(panel, typeof(Qualification)) => [
			width = 200
			items <=> "calificaciones"
			value <=> "seleccionado"
		]
		
		new Column<Qualification>(tabla) => [
			title = "Evaluado"
			fixedSize = 150
			bindContentsToProperty("evaluado")
		]
		
		new Column<Qualification>(tabla) => [
			title = "Puntos"
			fixedSize = 50
			bindContentsToProperty("puntos")
		]
		
		new Column<Qualification>(tabla) => [
			title = "Fecha"
			fixedSize = 150
			bindContentsToProperty("fecha").transformer = [LocalDateTime fecha |  
																		fecha.format(DateTimeFormatter.ISO_DATE) 
																		+ fecha.format(DateTimeFormatter.ISO_TIME)
			]
		]
		
		new Column<Qualification>(tabla) => [
			title = "User"
			fixedSize = 50
			bindContentsToProperty("user")
		]
		
		new Column<Qualification>(tabla) => [
			title = "Es Ofensiva"
			fixedSize = 100
			bindContentsToProperty("tieneContenidoOfensivo")
		]
	}
	
}