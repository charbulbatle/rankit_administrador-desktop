package org.charbulbatle.rankit.desktop.window

import java.awt.Color
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import org.charbulbatle.rankit.app_models.UserAppModel
import org.charbulbatle.rankit.desktop.components.LabeledCheckBox
import org.charbulbatle.rankit.desktop.components.LabeledValue
import org.charbulbatle.rankit.domain.User
import org.uqbar.arena.bindings.ValueTransformer
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.widgets.tables.Table
import org.uqbar.arena.windows.WindowOwner

import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import org.charbulbatle.rankit.app_models.CreateUserAppModel
import org.charbulbatle.rankit.desktop.dialog.NewUserDialog
import org.charbulbatle.rankit.app_models.QualificationAppModel
import org.charbulbatle.rankit.desktop.dialog.DeleteDialog
import org.uqbar.arena.windows.ErrorsPanel

class UserManagementWindow extends AdministrationWindow<UserAppModel>{
	
	
	new(WindowOwner owner, UserAppModel model) {
		super(owner, model)
	}
	
	override crearBotonDeNuevo(Panel panel) {
		new Button(panel) => [
			caption  = "Nuevo"
			width = 365
			onClick [ | new NewUserDialog(this, new CreateUserAppModel(modelObject)).open]
		]
	}
	
	override  crearPanelDeAcciones(Panel panel) {
		
		new Label(panel) =>[
			text = "Fecha de Registro: "
			fontSize = 12
		] 
		
		new Label(panel) => [
			width = 400
			(value <=> "seleccionado.fechaDeRegistro").transformer = new LocalDateTimeTransformer
		]
		
		new LabeledCheckBox(panel, "Activo") => [
			checkbox.value <=> "activo"
			checkbox.enabled <=> "noBaneado"
			checkbox.enabled <=> "haySeleccionado"
		]
		
		new LabeledCheckBox(panel, "Baneado") => [
			checkbox.value <=> "baneado"
			checkbox.enabled <=> "haySeleccionado"
		]
		
		new Label(panel).text = "Ultima Publicacion: "
		
		new Label(panel) => [
			(value <=> "seleccionado.ultimaPublicacion").transformer = new LocalDateTimeTransformer
		]
		
		new Button(panel) => [
			caption  = "Revisar Calificaciones"
			onClick [| new QualificationsManagementWindow(this,new QualificationAppModel() => [usuario = modelObject.seleccionado.nombre]).open]
			enabled <=> "haySeleccionado"
		]
		
		new Button(panel) => [
			caption  = "Blanquear Clave"
			onClick [| this.modelObject.seleccionado.blanquearClave; showInfo("Su nueva clave es: 1234") ]
			enabled <=> "haySeleccionado"
		]
		
		new Button(panel) => [
			caption  = "Eliminar"
			onClick [ | new DeleteDialog(this, this.modelObject).open]
			enabled <=> "haySeleccionado"
		]
	}
	
	override crearTabla(Panel panel) {		
		val tabla = new Table<User>(panel, typeof(User)) => [
			width = 200
			items <=> "usuarios"
			value <=> "seleccionado"
		]
		// Columnas ///////////////////////////////
		new Column<User>(tabla) => [
			title = "Fecha de Registro"
			fixedSize = 150
			bindContentsToProperty("fechaDeRegistro").transformer = [LocalDateTime fecha |  
																		fecha.format(DateTimeFormatter.ISO_DATE) 
																		+ fecha.format(DateTimeFormatter.ISO_TIME)
			]
		]
		
		new Column<User>(tabla) => [
			title = "Nombre"
			fixedSize = 100
			bindContentsToProperty("nombre")
		]
		
		new Column<User>(tabla) => [
			title = "Activo"
			fixedSize = 50
			bindContentsToProperty("activo")
		]
		
		new Column<User>(tabla) => [
			title = "Baneado"
			fixedSize = 50
			bindContentsToProperty("baneado")
		]
		
		// ////////////////////////////////////////
	}
	
	override crearBuscador(Panel mainPanel) {
		val contenedorUsuario = new Panel(mainPanel) => [
			layout = new HorizontalLayout
		]
		
		new Label(contenedorUsuario) => [
			text 	 = "Usuarios"
			fontSize = 16
		]
		
		// Buscador /////////////////////////////////
		val buscador = new Panel(mainPanel) =>[
			layout = new HorizontalLayout
		]
		
		new Label(buscador) => [
			text = "Buscar por nombre de usuario"
		]
		
		// Input field
		new TextBox(buscador) => [
			width = 200
			value <=> "buscado"
		]
		// /////////////////////////////////////////
	}
	
	override crearResumenDeSituacion(Panel mainPanel) {
		
		val panelHorizontal = new Panel(mainPanel) =>[
			layout = new HorizontalLayout
		]
		
		// Usuarios Registrados
		new LabeledValue(panelHorizontal, Color.BLUE) => [
			label.text = "Usuarios Registrados"
			value.value <=> "usuariosRegistrados"
		]
		
		// Usuarios Activos
		new LabeledValue(panelHorizontal, Color.BLUE) => [
			label.text = "Activos"
			value.value <=> "usuariosActivos"
		]
		
		// Usuarios Inactivos
		new LabeledValue(panelHorizontal, Color.RED) => [
			label.text = "Inactivos"
			value.value <=> "usuariosInactivos"
		]
		
		// Usuarios Baneados
		new LabeledValue(panelHorizontal, Color.RED) => [
			label.text = "Baneados"
			value.value <=> "usuariosBaneados"
		]
	}
	
	override crearFeedBackTitle(Panel panel) {
		// Nombre /////////////////////////////
		val nombre = new Panel(panel) => [
			layout = new HorizontalLayout
		] 
		
		new Label(nombre) => [
			fontSize = 14
			text = "Nombre: "
		]
		
		new Label(nombre) => [
			fontSize = 12
			value <=> "seleccionado.nombre"
		]
		// ///////////////////////////////////
		
		// titulo
		this.title = "RankIt --> Adm Usuarios"
	}
	
}

class LocalDateTimeTransformer implements ValueTransformer<LocalDateTime, String>{
				
	override getModelType() {
		typeof(LocalDateTime)
	}
	
	override getViewType() {
		typeof(String)
	}
	
	override modelToView(LocalDateTime valueFromModel) {
		valueFromModel.format(DateTimeFormatter.ISO_DATE) 
		+ valueFromModel.format(DateTimeFormatter.ISO_TIME)
	}
	
	override viewToModel(String valueFromView) {
		
	}
	
}
