package org.charbulbatle.rankit.desktop.window

import org.charbulbatle.rankit.app_models.RankItAppModel
import org.charbulbatle.rankit.desktop.components.LabeledButton
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.SimpleWindow
import org.uqbar.arena.windows.WindowOwner
import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Button
import org.charbulbatle.rankit.desktop.components.ThreeLabelButton
import org.charbulbatle.rankit.desktop.components.TwoLabelButton

/**
 * El menu que permite el acceso a todos los paneles de administracion
 */
class Menu extends SimpleWindow<RankItAppModel>{
	
	private static final  String ADM = "Adm. "
	
	new(WindowOwner owner, RankItAppModel model){
		super(owner, model)
	}
	
	new(WindowOwner owner) {
		super(owner, new RankItAppModel)
	}	
	
	override protected createErrorsPanel(Panel mainPanel) {
		//
	}
	
	override protected addActions(Panel botonera) {
		
		//Administracion de Usuarios
		
		new ThreeLabelButton(botonera, "Usuarios", "userAppModel.usuariosActivos", "userAppModel.usuariosRegistrados", "userAppModel.usuariosBaneados") => [
			button.onClick[| new UserManagementWindow(this, this.modelObject.userAppModel).open]
		]
		
		//Administracion de Calificaciones
		
		new TwoLabelButton(botonera, ADM + "Calificaciones", "qualificationAppModel.calificacionesTotales", "qualificationAppModel.calificacionesNoOfensivas") =>[
			button.onClick[| new QualificationsManagementWindow(this, this.modelObject.qualificationAppModel).open]
		]
		
		//Administracion de Servicios
		new TwoLabelButton(botonera, ADM + "Servicios", "serviceAppModel.serviciosInscriptos", "serviceAppModel.serviciosHabilitados") => [
			button.onClick[| new ServiceManagementWindow(this, this.modelObject.serviceAppModel).open]
		]
		
		//Administracion de Lugares
		new TwoLabelButton(botonera, ADM + "Lugares", "placesAppModel.serviciosInscriptos", "placesAppModel.serviciosHabilitados") => [
			button.onClick[| new PlaceManagementWindow(this, this.modelObject.placesAppModel).open]
		]
		
	}
	
	override protected createFormPanel(Panel mainPanel) {
		// Titulo
		this.title = "Rank-It!"
		var header = new Label(mainPanel) => [
			text 		= "Rank-It! Modulo de Administracion"
			fontSize 	= 16
		] 
		
		// body
		new Label(mainPanel) => [
			text = "Desde este modulo vas a poder gestionar los datos y opciones de la aplicacion. \n" +
				   "Como sos una persona de confianza vas a tener acceso a todo! \nSiempre acordate: Con un gran poder viene una gran responsabilidad"
			fontSize = 12
		] 
	}
	
}