package org.charbulbatle.rankit.desktop.window

import org.uqbar.arena.layout.ColumnLayout
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.ErrorsPanel
import org.uqbar.arena.windows.SimpleWindow
import org.uqbar.arena.windows.WindowOwner

abstract class AdministrationWindow<T> extends SimpleWindow<T>{
	
	new(WindowOwner owner, T model) {
		super(owner, model)
	}

	
	override protected addActions(Panel actionsPanel) {
		
	}
	
	
	override createFormPanel(Panel mainPanel) {
		
		
		// Header
		new Label(mainPanel) => [
			text = "Resumen de situacion:"
			fontSize = 16
		]
		
		crearResumenDeSituacion(mainPanel)
		
		crearBuscador(mainPanel)
		
		val columnLayout = new Panel(mainPanel) => [
			layout = new ColumnLayout(2)
		]
		
		val izq = new Panel(columnLayout)
		val der = new Panel(columnLayout)
		
		crearTabla(izq)
		
		crearFeedBackTitle(der)
		
		crearFeedBackPanel(der)
		
		crearPanelDeAcciones(der)
		
		crearBotonDeNuevo(izq)
		
		new Button(mainPanel) => [
			caption = "Cerrar Ventana"
			onClick [| this.close]
		]
		
	}
	
	override protected createErrorsPanel(Panel mainPanel) {
		//
	}
	
	def ErrorsPanel crearFeedBackPanel(Panel panel){
		this.taskDescription = "Edita la informacion"
		new ErrorsPanel(panel, this.taskDescription)
	}
	
	def void crearFeedBackTitle(Panel panel)
	
	def void crearResumenDeSituacion(Panel panel)
	
	def void crearBuscador(Panel panel)
	
	def void crearBotonDeNuevo(Panel panel)
	
	def void crearPanelDeAcciones(Panel panel) 
	
	def void crearTabla(Panel panel)
	
}