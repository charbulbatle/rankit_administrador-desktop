package org.charbulbatle.rankit.desktop.window

import java.awt.Color
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import org.charbulbatle.rankit.app_models.ServicesAppModel
import org.charbulbatle.rankit.desktop.components.LabeledCheckBox
import org.charbulbatle.rankit.desktop.components.LabeledValue
import org.charbulbatle.rankit.domain.Qualifiable
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.widgets.tables.Table
import org.uqbar.arena.windows.WindowOwner

import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import org.charbulbatle.rankit.app_models.QualificationAppModel
import org.charbulbatle.rankit.app_models.CreateServiceAppModel
import org.charbulbatle.rankit.desktop.dialog.NewServiceDialog
import org.charbulbatle.rankit.desktop.dialog.DeleteDialog

class ServiceManagementWindow extends AdministrationWindow<ServicesAppModel>{
	protected String tipo
	protected String tipoPlural
	
	new(WindowOwner owner, ServicesAppModel model) {
		super(owner, model)
		this.tipo = "Servicio"
		this.tipoPlural = "Servicios"
	}
	
//////////////////////////////////////////////////////////////////////////////////////////	
/////////////////////////////////// TABLA ////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
	override crearTabla(Panel panel) {
		val tabla = new Table<Qualifiable>(panel, typeof(Qualifiable)) => [
			width = 200
			items <=> "servicios"
			value <=> "seleccionado"
		]
		
/////////////////////////////////////////////////////////////////////////////////////////////				
////////////////////////////////////// COLUMNAS ////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////////////////////////////
        
		new Column<Qualifiable>(tabla) => [
			title = "Fecha de Registro"
			fixedSize = 100
			bindContentsToProperty("fechaDeRegistro").transformer = [LocalDateTime fecha |  
                                                                        fecha.format(DateTimeFormatter.ISO_DATE) 
                                                                        + fecha.format(DateTimeFormatter.ISO_TIME)
		                                                             ]
		                                                             
       ]
		
		new Column<Qualifiable>(tabla) => [
		    title = "Nombre"
			fixedSize = 100
			bindContentsToProperty("nombre")
		]
	
	   new Column<Qualifiable>(tabla) => [
	       title = "Habilitado"
	       fixedSize = 100
	       bindContentsToProperty("habilitado")
	   ]
	   
	}
	
	override crearBotonDeNuevo(Panel panel) {
		new Button(panel) => [
			caption  = "Nuevo"
			width = 365
			onClick [ | new NewServiceDialog(this, new CreateServiceAppModel(modelObject)).open]
		]
	}	
	
/////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////// BUSCADOR ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

    override crearBuscador(Panel mainPanel) {
        val contenedorUsuario = new Panel(mainPanel) => [
            layout = new HorizontalLayout
        ]
        
        new Label(contenedorUsuario) => [
            text     = tipoPlural
            fontSize = 16
        ]
        
        val buscador = new Panel(mainPanel) =>[
            layout = new HorizontalLayout
        ]
        
        new Label(buscador) => [
            text = "Buscar por nombre de " + tipo
        ]
        
        new TextBox(buscador) => [
            width = 200
            value <=> "buscado"
        ]

    }

////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// SELECCION ///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

    override crearFeedBackTitle(Panel panel) {

        val nombre = new Panel(panel) => [
            layout = new HorizontalLayout
        ] 
        
        new Label(nombre) => [
            fontSize = 14
            text = "Nombre: "
        ]
        
        new Label(nombre) => [
            fontSize = 12
            value <=> "seleccionado.nombre"
        ]
        
        // titulo
		this.title = "RankIt --> Adm " + tipoPlural
		// ///////////////////////////////////
    }

////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// PANEL DE ACCIONES ////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

	

   override crearPanelDeAcciones(Panel panel){
        new Label(panel) =>[
            text = "Nombre"
            fontSize = 14
        ]
        
        new Label(panel) =>[
             value <=> "seleccionado.nombre"
        ]
        
        new LabeledCheckBox(panel, "Habilitado") => [
        	checkbox.enabled <=> "haySeleccionado"
        	checkbox.value <=> "habilitado"
        ]
        
        new LabeledValue(panel, Color.BLACK) =>[
            label.text = "Rating promedio: "
            value.value <=> "promedio"
        ]
        
        new LabeledValue(panel, Color.BLACK) =>[
            label.text = "Calificaciones: "
            value.value <=> "calificaciones"
        ]
        /////////////////////////////////////////////////////
        ////////////////// BOTONES //////////////////////////
        /////////////////////////////////////////////////////
        new Button(panel) => [
            caption  = "Revisar Calificaciones"
            onClick[new QualificationsManagementWindow(this,new QualificationAppModel(modelObject.seleccionado.nombre)).open]
            enabled <=> "haySeleccionado"
        ]
        
        new Button(panel) => [
            caption  = "Eliminar"
            enabled <=> "haySeleccionado"
            width = 350
            onClick [ | new DeleteDialog(this, this.modelObject).open]
        ]
        
		        
    }
    
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////// RESUMEN DE SITUACION  //////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////    
    
	override crearResumenDeSituacion(Panel mainPanel) {
		val panelHorizontal = new Panel(mainPanel) =>[
			layout = new HorizontalLayout
		]
		
		// Usuarios Registrados
		new LabeledValue(panelHorizontal, Color.BLUE) => [
			label.text = tipoPlural + " Inscriptos"
			value.value <=> "serviciosInscriptos"
		]
		
		// Usuarios Activos
		new LabeledValue(panelHorizontal, Color.BLUE) => [
			label.text = "Habilitados"
			value.value <=> "serviciosHabilitados"
		]
		
		// Usuarios Inactivos
		new LabeledValue(panelHorizontal, Color.RED) => [
			label.text = "Deshabilitados"
			value.value <=> "serviciosDeshabilitados"
		]
		

	}



}