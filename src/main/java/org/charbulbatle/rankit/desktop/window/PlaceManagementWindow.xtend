package org.charbulbatle.rankit.desktop.window

import org.charbulbatle.rankit.app_models.PlacesAppModel
import org.uqbar.arena.windows.WindowOwner

class PlaceManagementWindow extends ServiceManagementWindow{
	
	new(WindowOwner owner, PlacesAppModel model) {
		super(owner, model)
		this.tipo 		= "Lugar"
		this.tipoPlural = "Lugares"
	}	
	
}