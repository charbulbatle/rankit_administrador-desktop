package org.charbulbatle.rankit.desktop

import org.uqbar.arena.Application
import org.charbulbatle.rankit.desktop.window.Menu
import org.charbulbatle.rankit.app_models.StubRankitAppModel

class RankItDesktopApplication extends Application{
	
	override protected createMainWindow() {
		
		new Menu(this, new StubRankitAppModel)
	}
	
	def static main(String[] args) {
		new RankItDesktopApplication().start()
	}
	
}