package org.charbulbatle.rankit.desktop.components

import org.uqbar.arena.widgets.Container
import org.uqbar.arena.widgets.Label
import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class ThreeLabelButton extends TwoLabelButton{
	private Label label3;
	
	new(Container container, String buttonCaption, String property1, String property2) {
		super(container, buttonCaption, property1, property2)
	}
	
	new(Container container, String buttonCaption, String property1, String property2, String property3){
		super(container, buttonCaption, property1, property2)
		new Label(labelContainer).text = "("
		new Label(labelContainer).value <=> property3
		new Label(labelContainer).text = ")"
	}
		
}