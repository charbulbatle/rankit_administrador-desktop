package org.charbulbatle.rankit.desktop.components

import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Container
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.Label
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.lacar.ui.model.Action

@Accessors
class LabeledButton extends Panel{
	
	private Button button
	private Panel mainPanel
	private Label label;
	
	new(Container container) {
		super(container)
		mainPanel 		= new Panel(container)
		button 			= new Button(mainPanel)
		label 			= new Label(mainPanel)
		
	}
	
	new(Container container, String buttonCaption){
		this(container)
		button.caption = buttonCaption
	}
	
}
