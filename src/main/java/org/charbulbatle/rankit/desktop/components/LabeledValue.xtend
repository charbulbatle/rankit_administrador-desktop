package org.charbulbatle.rankit.desktop.components

import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.Container
import org.uqbar.arena.widgets.Label
import java.awt.Color
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.arena.layout.HorizontalLayout

@Accessors
/**
 * A panel with two labels one for a title and the other to depict a value 
 */
class LabeledValue extends Panel{
	private Label label;
	private Label value;
	private Panel panel;
	
	/**
	 * @param color the color of the label containing the value
	 */
	new(Container container, Color color) {
		super(container)
		this.panel = new Panel(container) => [
			layout = new HorizontalLayout
		]
		this.label = new Label(panel)
		this.value = new Label(panel)
		value.foreground = color
	}
	
	new(Container container, Color color, String labelText) {
		this(container, color)
		this.label.text = labelText
	}
	
	
	
}