package org.charbulbatle.rankit.desktop.components

import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.Container
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.arena.layout.HorizontalLayout
import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*

/**
 * Un Panel que contiene un boton y dos Labels debajo del mismo
 */
@Accessors
class TwoLabelButton extends Panel{
	
	private Button button
	private Panel mainPanel
	private Panel labelContainer
	private Label label1;
	private Label label2;
	
	new(Container container) {
		super(container)
		mainPanel 		= new Panel(container)
		button 			= new Button(mainPanel)
		labelContainer	= new Panel(mainPanel)
		labelContainer.layout = new HorizontalLayout
		label1 			= new Label(labelContainer)
		new Label(labelContainer).text = "/"
		label2 			= new Label(labelContainer)
		
	}
	
	new(Container container, String buttonCaption, String property1, String property2){
		this(container)
		button.caption = buttonCaption
		label1.value <=> property1
		label2.value <=> property2
	}
	
	
}