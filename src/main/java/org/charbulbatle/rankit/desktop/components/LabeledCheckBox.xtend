package org.charbulbatle.rankit.desktop.components

import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.Container
import org.uqbar.arena.widgets.CheckBox
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.layout.HorizontalLayout
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class LabeledCheckBox extends Panel{
	
	private CheckBox checkbox
	private Label label
	
	new(Container container, String labelText) {
		super(container)
		val horizontalPanel = new Panel(container) => [
			layout = new HorizontalLayout()
		]
		
		checkbox = new CheckBox(horizontalPanel)
		label = new Label(horizontalPanel) => [
			text = labelText
		]
	}
	
	
	
}