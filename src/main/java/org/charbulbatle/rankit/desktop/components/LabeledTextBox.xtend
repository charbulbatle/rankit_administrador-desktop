package org.charbulbatle.rankit.desktop.components

import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.Container
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.layout.HorizontalLayout
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class LabeledTextBox extends Panel{
	private Label label
	private TextBox textBox
	private Panel horizontalPanel
	
	new(Container container, String labelText) {
		super(container)
		
		this.horizontalPanel = new Panel(container) =>[
			layout = new HorizontalLayout
		]
		
		this.label = new Label(horizontalPanel) => [
			text = labelText
		]
		
		// Input field
		this.textBox = new TextBox(horizontalPanel) => [
			width = 200
		]
	}
	
	
	
}